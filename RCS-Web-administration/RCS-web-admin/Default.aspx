﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RCS_web_admin.Default" %>
<asp:Content ID="cphHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="cphInfoStrip" ContentPlaceHolderID="cphInfoStrip" runat="server">
    
    <h2><img src="assets/images/icons/user_32.png" alt="Login" />Вход</h2>
	
</asp:Content>

<asp:Content ID="cphMessageStrip" runat="server" ContentPlaceHolderID="cphMessageStrip">
</asp:Content>

<asp:Content ID="cphContent" ContentPlaceHolderID="cphContent" runat="server">

    <div id="login">
					
		<div class="content-box">
			<div class="content-box-header">
				<h3>Вход</h3>
			</div>
					
			<div class="content-box-content">
						
				<div class="notification information">Въведете име и парола.</div>
						
                <asp:Login ID="frmLogin" runat="server" 
                    DestinationPageUrl="~/rcs/users/User-managment.aspx" 
                    LoginButtonText="Вход" TitleText="">
                </asp:Login>
			</div>
		</div><!-- end .content-box -->
	</div><!-- end #login -->

</asp:Content>
