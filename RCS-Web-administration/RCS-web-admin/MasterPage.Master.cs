﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RCS_web_admin
{
	public partial class MasterPage : System.Web.UI.MasterPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		public	string	getRootFolder()
		{
			string	root	= "http";
			if( Request.ServerVariables["HTTPS"].ToLower() == "on" )
				root	+= "s";
				
			root	+= "://" + Request.ServerVariables["HTTP_HOST"] + Request.ApplicationPath;

			if( ! root.EndsWith("/") )
				root += "/";

			return root;
		}
	}
}