﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer;
using BussinessLayer;


namespace RCS_web_admin
{
	public partial class Insert : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Registration_OnClick(object sender, EventArgs e)
        {
			Network	network		= new Network();
			network.FirstName	= TxtFName.Text;
            network.LastName	= TxtLName.Text;
            network.Name		= TxtUserName.Text;
            network.Email		= TxtEmail.Text;
            network.City		= TxtCity.Text;
            network.Address		= TxtAddress.Text;
            network.Password	= TxtPassword.Text;
			
			NetworksDataAccess	networksDA	= new NetworksDataAccess();
			network.ID		= networksDA.Insert( network );

            User user		= new User();
			user.Network	= network;
            user.Firstname	= TxtFName.Text;
            user.Lastname	= TxtLName.Text;
            user.Username	= TxtUserName.Text;
            user.Email		= TxtEmail.Text;
            user.City		= TxtCity.Text;
            user.Address	= TxtAddress.Text;
            user.Password	= TxtPassword.Text;

			//	When registering a network, the network owner is assigned as a network administrator.
			RolesDataAccess	rolesDA	= new RolesDataAccess();
			Role			role	= rolesDA.selectRoleByID( 2 );	// Network administrator role has ID 2 in the DB.

			user.Roles	= new List<Role>();
			user.Roles.Add( role );

            UsersDataAccess userDA = new UsersDataAccess();
            userDA.Insert(user);


			TxtAddress.Text		= string.Empty;
			TxtFName.Text		= string.Empty;
			TxtLName.Text		= string.Empty;
			TxtUserName.Text	= string.Empty;
			TxtEmail.Text		= string.Empty;
			TxtCity.Text		= string.Empty;
			TxtPassword.Text	= string.Empty;

			lblStatus.Text		= "Вашата регистрация беше успешна!";
        }
	}
    

    
}