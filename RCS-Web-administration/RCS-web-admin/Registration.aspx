﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="RCS_web_admin.Insert" %>
<asp:Content ID="cphHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="cphInfoStrip" ContentPlaceHolderID="cphInfoStrip" runat="server">
    
    <h2><img src="../../assets/images/icons/user_32.png" alt="Login" />Регистрация</h2>
	
</asp:Content>

<asp:Content ID="cphMessageStrip" runat="server" ContentPlaceHolderID="cphMessageStrip">
</asp:Content>

<asp:Content ID="cphContent" ContentPlaceHolderID="cphContent" runat="server">

    <div id="registration">
		<div class="content-box">
			<div class="content-box-header">
				<h3>Форма за регистрация</h3>
			</div>
					
			<div class="content-box-content">
						
				<div class="notification information"><span style="margin-left: 25px"><asp:Label ID="lblStatus" runat="server"  Text="Моля попълнете Вашите данни!" /></span></div>
					<div>
                        <table>
                              <tr>
                <td>Име:</td>
                <td>
                    <asp:TextBox ID="TxtFName" runat="server"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td>Фамилия:</td>
                <td>
                    <asp:TextBox ID="TxtLName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Потребителско име:</td>
                <td>
                    <asp:TextBox ID="TxtUserName" runat="server"></asp:TextBox>
                </td>
            </tr>
          
            <tr>
                <td>Парола:</td>
                <td>
                    <asp:TextBox ID="TxtPassword" runat="server"
                                 TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Повторете паролата:</td>
                <td>
                    <asp:TextBox ID="TxtRePassword" runat="server"
                                 TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Имейл:</td>
                <td>
                    <asp:TextBox ID="TxtEmail" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Адрес:</td>
                <td>
                    <asp:TextBox ID="TxtAddress" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Град</td>
                <td>
                    <asp:TextBox ID="TxtCity" runat="server"></asp:TextBox>
                </td>
            </tr>
                        </table>
                            <asp:RegularExpressionValidator id="regvUsername"
                                                            ControlToValidate = "txtUsername"
                                                            Display = "None"
                                                            ValidationExpression  = "^[a-zA-Zа-яА-Я0-9_-]{4,15}$"
                                                            ErrorMessage  = "Потребителското име може да съдържа само букви, цифри, и символите '_' или '-'. Минимална дължина 4 символа, максимална - 15." 
                                                            runat = "server"
                                                            />
                            <asp:RequiredFieldValidator    ID="rfvUsername" runat="server"
                                                           ErrorMessage="Потребителското име може да съдържа само букви, цифри, и символите '_' или '-'. Минимална дължина 4 символа, максимална - 15."
                                                           ControlToValidate="txtUsername"
                                                           Display="None"
                                                            />
                             <asp:RegularExpressionValidator id="regvPassword" 
                                                            ControlToValidate="txtPassword"
                                                            Display="None"
                                                            ValidationExpression="^[a-zA-Z0-9#%^&<>?]{6,17}$"
                                                            ErrorMessage="Паролата може да съдържа само букви на латиница, цифри и символите '#', '%', '^', '&', '<', '>', '?'. Минимална дължина 6 символа, максимална - 17."
                                                            runat="server"
                                                              />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server"
                                                            ErrorMessage="Паролата може да съдържа само букви на латиница, цифри и символите '#', '%', '^', '&', '<', '>', '?'. Минимална дължина 6 символа, максимална - 17."
                                                            ControlToValidate="txtPassword"
                                                            Display="None"
                                                             />
                           
                     <asp:RegularExpressionValidator id="regvFirstName" 
                                                            ControlToValidate="txtFName"
                                                            Display="None"
                                                            ValidationExpression="^[a-zA-Zа-яА-Я]{1,255}$"
                                                            ErrorMessage="Невалидно име за потребител."
                                                            runat="server"
                                                            />
                                     <asp:RequiredFieldValidator ID="rfvFirstName" runat="server"
                                                            ErrorMessage="Невалидно име за потребител."
                                                            ControlToValidate="txtFName"
                                                            Display="None"
                                                            />
                     <asp:RegularExpressionValidator id="regvLastName" 
                                                            ControlToValidate="txtLName"
                                                            Display="None"
                                                            ValidationExpression="^[a-zA-Zа-яА-Я]{1,255}$"
                                                            ErrorMessage="Невалидна фамилия за потребител."
                                                            runat="server"
                                                            />
                                     <asp:RequiredFieldValidator ID="rfvLastName" runat="server"
                                                            ErrorMessage="Невалидна фамилия за потребител."
                                                            ControlToValidate="txtLName"
                                                            Display="None"
                                                            />
                    <asp:RegularExpressionValidator id="regvEmail" 
                                                            ControlToValidate="txtEmail"
                                                            Display="None"
                                                            ValidationExpression="^[a-zA-z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$"
                                                            ErrorMessage="Невалиден email адрес за потребил."
                                                            runat="server"
                                                            />
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server"
                                                            ErrorMessage="Невалиден email адрес за потребител."
                                                            ControlToValidate="txtEmail"
                                                            Display="None"
                                                            />
                     <asp:RegularExpressionValidator id="regvAddress" 
                                                            ControlToValidate="txtAddress"
                                                            Display="None"
                                                            ValidationExpression="^[a-zA-Zа-яА-Я0-9\. ]{1,255}$"
                                                            ErrorMessage="Невалиден адрес за потребител."
                                                            runat="server"
                                                            />
                                      <asp:RequiredFieldValidator ID="rfvAddress" runat="server"
                                                            ErrorMessage="Невалиден адрес за потребител."
                                                            ControlToValidate="txtAddress"
                                                            Display="None"
                                                            />
                    <asp:RegularExpressionValidator id="regvCity" 
                                                            ControlToValidate="TxtCity"
                                                            Display="None"
                                                            ValidationExpression="^[a-zA-Zа-яА-Я]{1,255}"
                                                            ErrorMessage="Невалиден град за потребител"
                                                            runat="server"
                                                            />
                                      <asp:RequiredFieldValidator ID="rfvCity" runat="server"
                                                            ErrorMessage="Невалиден град за потребител"
                                                            ControlToValidate="TxtCity"
                                                            Display="None"
                                                            />
                                        <asp:CompareValidator
                                            ID="cmpVldr" runat="server"
                                            Display="None" 
                                            ErrorMessage="Паролите не съвпадат!" ControlToCompare="TxtPassword" ControlToValidate="TxtRePassword"></asp:CompareValidator>
                     <asp:ValidationSummary ID="vsUpdate" 
                        runat="server"
                        ShowMessageBox="False" ShowSummary="True" DisplayMode="BulletList"
                        CssClass="validation"
                       />
                    </div>	
                    <div>
                        <asp:Button ID="RegistrationBtn" runat="server" UseSubmitBehavior="true" Text="Регистрирай ме" OnClick="Registration_OnClick" />
                    </div>
			</div>
		</div><!-- end .content-box -->
	</div><!-- end #login -->

</asp:Content>
