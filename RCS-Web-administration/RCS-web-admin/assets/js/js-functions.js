﻿
function showPopUp( url, width, height, reload )
{
	$.fancybox( {
		'speedIn' : '2000',
		'speedOut' : '2000',
		'width' : width,
		'height' : height,
		'opacity'		: true,
		'overlayShow'	: true,
		'hideOnOverlayClick' : false,
		'transitionIn'	: 'elastic',
		'transitionOut'	: 'elastic',
		'href' : url,
		'type' : 'iframe',
		'onCleanup': function () {
			parent.location.reload(reload);
			}
		} );
}