﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RCS_web_admin.rcs.settings.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInfoStrip" runat="server">
    <h2><img src="../../assets/images/icons/user_32.png" alt="Login" />Моето лого</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMessageStrip" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContent" runat="server">
<div>
    <div style="float:left;">
        <asp:FileUpload ID="fuImage" runat="server"></asp:FileUpload><br />
        <asp:Button ID="btnInsert" runat="server" CssClass="button" Text="Добави" OnClick="btnInsert_Click" />
        <br /> <br />
        <br /> <br />
        <asp:PlaceHolder ID = "plhStatus" runat = "server">
        <asp:Label ID = "lblStatus" runat = "server" CssClass = "warning" Visible = "false" ></asp:Label>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID = "plhError" runat = "server" Visible = "false">
        <asp:Label ID = "lblError" runat = "server" CssClass = "error"></asp:Label>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID = "plhSuccess" runat = "server" Visible = "false">
        <asp:Label ID = "lblSuccess" runat = "server" CssClass = "success"></asp:Label>
        </asp:PlaceHolder>
    </div>
    <div style="float:right;">
        <img src='loadNetworkImage.aspx?id=<%=Session["networkID"].ToString() %>' alt="" width="32px" height="32px" />
    </div>
</div>
</asp:Content>
