﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessLayer;
using System.IO;
using DataAccessLayer;

namespace RCS_web_admin.rcs.settings
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}
		protected	void	btnInsert_Click( object sender, EventArgs e )
		{
			NetworkCustomImage	imgInfo				= new NetworkCustomImage();
			NetworkCustomImagesDataAccess	imgDA	= new NetworkCustomImagesDataAccess();
			int netID			= (int)Session["networkID"];
			bool hasPrevious	= false;

			try
			{
				if( ! fuImage.PostedFile.ContentType.Contains("image") )
				{
					plhStatus.Visible	= false;
					plhSuccess.Visible	= false;

					lblError.Text = "Невалиден файлов формат.";
					plhError.Visible	= true;
					return;
				}

				//	Check previous records.
				NetworkCustomImage	imgPrev				= imgDA.SelectNetworkImage( netID );
				if( imgPrev != null )
					hasPrevious = true;

				Stream	strm		= fuImage.PostedFile.InputStream;
				int		fileSize	= fuImage.FileBytes.Length;

				byte[] content = new byte[fileSize];
				strm.Read(content, 0, fileSize);

				imgInfo.Content		= content;
				imgInfo.Name		= fuImage.PostedFile.FileName;
				imgInfo.Size		= fileSize;
				imgInfo.Extension	= fuImage.PostedFile.ContentType;

				if( ! hasPrevious )
					imgDA.Insert( imgInfo, netID );
				else
					imgDA.Update( imgInfo, netID );

				lblSuccess.Text		= "Логото беше успешно прикачено!";
                plhSuccess.Visible	= true;

                plhStatus.Visible	= false;
                plhError.Visible	= false;

			}
			catch( Exception ex )
			{
				plhStatus.Visible	= false;
				plhSuccess.Visible	= false;
					
				lblError.Text		= ex.Message;
				plhError.Visible	= true;
			}
		}
	}
}