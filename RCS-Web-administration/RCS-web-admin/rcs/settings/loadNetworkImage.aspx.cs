﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DataAccessLayer;
using BussinessLayer;

namespace RCS_web_admin.rcs.settings
{
	public partial class loadNetworkImage : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			int nID	= Convert.ToInt32(Request.Params["id"]);

            NetworkCustomImagesDataAccess nciDA = new NetworkCustomImagesDataAccess();
			NetworkCustomImage	nImage			= nciDA.SelectNetworkImage( nID );

            if( nImage != null )
            {
                Response.ContentType = nImage.Extension;
                Response.BinaryWrite(nImage.Content);
                Response.Flush();
            }
            else
            {
                System.Drawing.Image imageIn = System.Drawing.Image.FromFile(Server.MapPath("images/backgr.png"));

                Response.ContentType = "image/png";
                Response.BinaryWrite(imageToByteArray( imageIn ));
                Response.Flush();
            }
        }
        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }
	}
}