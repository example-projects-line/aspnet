﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewNetworkUsers.aspx.cs" Inherits="RCS_web_admin.rcs.networks.ViewNetworkUsers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxControlToolkit" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title></title>
    
	<link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/secondary.css" />

</head>
<body>
    <form id="frmMain" runat="server">
    <div>
        <asp:ScriptManager ID="scrManager" runat="server"></asp:ScriptManager>
         <asp:GridView ID="grvUsers" runat="server" CssClass="mGrid" AllowPaging="true"
                          DataSourceID = "odsUsers"
                          AutoGenerateColumns="false"
                          PagerStyle-CssClass="pgr"
                        AlternatingRowStyle-CssClass="alt"
                        DataKeyNames="ID,Username,Firstname,Lastname,Address,City" style="margin: 0px auto;"
                        >
                        <Columns>
                        <asp:TemplateField HeaderText="Идент. № " >
                          <ItemTemplate><%# Eval("ID") %></ItemTemplate>
                          </asp:TemplateField>
                           <asp:TemplateField HeaderText="Потребителско име" >
                          <ItemTemplate><%# Eval("Username") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Име на потребителя" >
                          <ItemTemplate><%# Eval("Firstname") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Фамилия на потребителя" >
                          <ItemTemplate><%# Eval("Lastname") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Адрес" >
                          <ItemTemplate><%# Eval("Address") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Град" >
                          <ItemTemplate><%# Eval("City") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField>
                           <ItemTemplate>
                                <asp:ImageButton runat="server"
                                     ImageAlign="Middle"
                                     ImageUrl ="~/assets/images/delete.png"
                                     ID="btnDelete" CommandName = "Delete"
                                     Text= "Delete"></asp:ImageButton>
                           <AjaxControlToolkit:ConfirmButtonExtender ID="cbeDelete" runat="server"
                            TargetControlID="btnDelete" DisplayModalPopupID="mpeDelete"
                            />
                            <AjaxControlToolkit:ModalPopupExtender
                                ID="mpeDelete"
                                runat="server"
                                CancelControlID="cancel"
                                PopupControlID="popup"
                                OkControlID="btnOK"
                                TargetControlID="btnDelete"
                                BackgroundCssClass="modalBackground"
                                />
                                 <asp:Panel ID="popup" runat="server" CssClass="modalPopup" style="display:none;">
                                    <br />
                                <p>Сигурни ли сте, че искате да изтриете записа?</p>
                                <asp:Button ID="btnOK" runat="server" Text="Да" CssClass="button" />
                                <asp:Button ID="cancel" runat="server" Text="Отказ" CssClass="button" />
                            </asp:Panel>
                           </ItemTemplate>
                          </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
            <asp:ObjectDataSource ID="odsUsers" runat="server" 
                        EnableViewState="False"
                        SelectMethod = "SelectAllNetworkUsers"
                        DeleteMethod = "Delete"
                        TypeName = "DataAccessLayer.UsersDataAccess"
                        DataObjectTypeName = "BussinessLayer.User"
                        >
                        <SelectParameters>
                            <asp:QueryStringParameter Name="networkID" QueryStringField="id" Type="Int32" DefaultValue="-1" />
                        </SelectParameters>
                        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
