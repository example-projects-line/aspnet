﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ViewLicenses.aspx.cs" Inherits="RCS_web_admin.rcs.networks.ViewLicenses" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInfoStrip" runat="server">
    <h2><img src="../../assets/images/icons/user_32.png" alt="" />Лицензи</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMessageStrip" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContent" runat="server">  
<asp:ScriptManager ID="scrManager" runat="server"></asp:ScriptManager>
        <div class="content-box">
	    <div class="content-box-header">
		    <h3>Регистрирани лицензи</h3>
	    </div>
					
	    <div class="content-box-content">
            <asp:GridView ID="grvLicenses" runat="server"
                AutoGenerateColumns = "false"
                DataSourceID="odsLicenses"
                CssClass="mGrid"
                DataKeyNames="ID, Date_Created, Date_Expired, Confirmed" style="margin: 0px auto;"
            >
                <Columns>
                     <asp:TemplateField HeaderText="Идент. №">
                    <ItemTemplate><%# Eval("ID") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Мрежа">
                    <ItemTemplate><%# Eval("Network.Name")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Потребителски лицензи (бр.)">
                    <ItemTemplate><%# Eval("User_Counts")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Дата на регистриране">
                    <ItemTemplate><%# Eval("Date_Created", "{0:dd-MMM-yyyy г.}")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Крайна дата">
                    <ItemTemplate><%# Eval("Date_Expired", "{0:dd-MMM-yyyy г.}")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Потвърден">
                    <ItemTemplate><asp:CheckBox ID="chbConfirmed" Enabled="false" runat="server" Checked='<%# Eval("Confirmed") %>' /></ItemTemplate>
                </asp:TemplateField>
               <%-- <asp:TemplateField>
                    <ItemTemplate>
                        <input type="button" value="" class="button-info" onclick="showPopUp( 'OrderItems.aspx?id= <%# Eval("ID")%>', 800, 600, true );" />
                    </ItemTemplate>
                </asp:TemplateField>--%>
               <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton runat="server" 
                            ImageAlign="Middle"
                            ImageUrl="~/assets/images/delete.png" 
                            ID="btnDelete" CommandName="Delete" 
                            Text="Delete"></asp:ImageButton>
                        <AjaxControlToolkit:ConfirmButtonExtender ID="cbeDelete" runat="server"
                        TargetControlID="btnDelete" DisplayModalPopupID="mpeDelete"
                        />
                        <AjaxControlToolkit:ModalPopupExtender
                            ID="mpeDelete"
                            runat="server"
                            CancelControlID="cancel"
                            PopupControlID="popup"
                            OkControlID="btnOK"
                            TargetControlID="btnDelete"
                            BackgroundCssClass="modalBackground"
                            />
                        <asp:Panel ID="popup" runat="server" CssClass="modalPopup" style="display:none;">
                                <br />
                            <p>Сигурни ли сте, че искате да изтриете записа?</p>
                            <asp:Button ID="btnOK" runat="server" Text="Да" CssClass="button" />
                            <asp:Button ID="cancel" runat="server" Text="Отказ" CssClass="button" />
                        </asp:Panel>
                    </ItemTemplate>
                </asp:TemplateField>
                <%-- 
                <asp:TemplateField>
                    <ItemTemplate>
                        <input type="button" value="" class="button-edit" onclick="showPopUp( 'Update.aspx?id= <%# Eval("ID")%>', 450, 400, true );" />
                    </ItemTemplate>
                </asp:TemplateField>--%>
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsLicenses" runat="server"
                DeleteMethod="Delete"
                SelectMethod="SelectAllLicenses"
                UpdateMethod="Update"
                TypeName="DataAccessLayer.LicensesDataAccess"
                DataObjectTypeName="BussinessLayer.License"
            >
            </asp:ObjectDataSource>
        </div>
    </div>
</asp:Content>
