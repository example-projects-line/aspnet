﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RCS_web_admin.rcs.networks.Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInfoStrip" runat="server">
    <h2><img src="../../assets/images/icons/user_32.png" alt="" />Мрежи</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMessageStrip" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContent" runat="server">
<asp:ScriptManager ID="scrManager" runat="server"></asp:ScriptManager>
    <div class="content-box">
	    <div class="content-box-header">
		    <h3>Регистрирани мрежи</h3>
	    </div>
					
	    <div class="content-box-content">
            <asp:GridView ID="grvNetworks" runat="server" CssClass="mGrid" AllowPaging="true"
                          DataSourceID = "odsNetworks"
                          AutoGenerateColumns="false"
                          PagerStyle-CssClass="pgr"
                        AlternatingRowStyle-CssClass="alt"
                        DataKeyNames="ID,Name,Firstname,Lastname,Address,City" style="margin: 0px auto;"
                        >
                        <Columns>
                        <asp:TemplateField HeaderText="Идент. № " >
                          <ItemTemplate><%# Eval("ID") %></ItemTemplate>
                          </asp:TemplateField>
                           <asp:TemplateField HeaderText="Име на мрежата" >
                          <ItemTemplate><%# Eval("Name") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Име на потребителя" >
                          <ItemTemplate><%# Eval("Firstname") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Фамилия на потребителя" >
                          <ItemTemplate><%# Eval("Lastname") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Адрес" >
                          <ItemTemplate><%# Eval("Address") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Град" >
                          <ItemTemplate><%# Eval("City") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField>
                           <ItemTemplate>
                                <asp:ImageButton runat="server"
                                     ImageAlign="Middle"
                                     ImageUrl ="~/assets/images/delete.png"
                                     ID="btnDelete" CommandName = "Delete"
                                     Text= "Delete"></asp:ImageButton>
                           <AjaxControlToolkit:ConfirmButtonExtender ID="cbeDelete" runat="server"
                            TargetControlID="btnDelete" DisplayModalPopupID="mpeDelete"
                            />
                            <AjaxControlToolkit:ModalPopupExtender
                                ID="mpeDelete"
                                runat="server"
                                CancelControlID="cancel"
                                PopupControlID="popup"
                                OkControlID="btnOK"
                                TargetControlID="btnDelete"
                                BackgroundCssClass="modalBackground"
                                />
                                 <asp:Panel ID="popup" runat="server" CssClass="modalPopup" style="display:none;">
                                    <br />
                                <p>Сигурни ли сте, че искате да изтриете записа?</p>
                                <asp:Button ID="btnOK" runat="server" Text="Да" CssClass="button" />
                                <asp:Button ID="cancel" runat="server" Text="Отказ" CssClass="button" />
                            </asp:Panel>
                           </ItemTemplate>
                          </asp:TemplateField>
                           <asp:TemplateField>
                                <ItemTemplate>
                                    <input type="button" value="Виж потребителите" class="button" onclick="showPopUp( 'ViewNetworkUsers.aspx?id= <%# Eval("ID")%>', 750, 600, false );" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
            <asp:ObjectDataSource ID="odsNetworks" runat="server" 
                        EnableViewState="False"
                        SelectMethod = "SelectAllNetworks"
                        DeleteMethod = "Delete"
                        TypeName = "DataAccessLayer.NetworksDataAccess"
                        DataObjectTypeName = "BussinessLayer.Network"
                        >
                        </asp:ObjectDataSource>
        </div>
    </div>
</asp:Content>
