﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer;
using BussinessLayer;

namespace RCS_web_admin.rcs.sessions
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private User m_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                load();
        }

         private void load()
        {
            int userID = -1;

            if ( Request.Params["id"] != null )
            userID  = Convert.ToInt32(Request.Params["id"] ) ;

            if(userID != -1)
            {
                DataAccessLayer.UsersDataAccess userDA = new DataAccessLayer.UsersDataAccess();
                m_user = userDA.selectUserByID ( userID ) ;
                List<BussinessLayer.User> users = new List<BussinessLayer.User>();

                if(m_user != null)
                {
                    users.Add ( m_user );

                    grvSessions.DataSource = users;
                    grvSessions.DataBind();
                }

            }
        }
         protected void btnUpdate_Click(object sender, EventArgs e)
         {

             License license = new License();

             
             TextBox txtchbConfirmed = (TextBox)Page.Form.FindControl("grvSessions:txtchbConfirmed");

             License licence = new License();

             licence.Confirmed = Convert.ToBoolean(txtchbConfirmed);

             try
             {
                 LicensesDataAccess lcsDA = new LicensesDataAccess();

                 lcsDA.ConfirmedUpdate(licence);

                 lblSuccess.Text = "Данните на клиента бяха успешно променени!";
                 plhSuccess.Visible = true;

                 plhStatus.Visible = false;
                 plhError.Visible = false;

             }
             catch (Exception ex)
             {
                 lblError.Text = ex.Message;

                 plhStatus.Visible = false;
                 plhSuccess.Visible = false;
                 plhError.Visible = true;

             }
         }
    }
}