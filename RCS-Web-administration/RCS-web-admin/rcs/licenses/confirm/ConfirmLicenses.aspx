﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConfirmLicenses.aspx.cs" Inherits="RCS_web_admin.rcs.sessions.WebForm1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInfoStrip" runat="server">
    <h2><img src="../../../assets/images/icons/user_32.png" alt="Login" />Непотвърдени лицензи</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMessageStrip" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContent" runat="server">

    <asp:ScriptManager ID="scrManager" runat="server"></asp:ScriptManager>
        <div class="content-box">
            <div class="content-box-header">
                <h3>Чакащи лицензи по мрежи</h3>
            </div>

            <div class="content-box-content">
                <asp:GridView ID="grvSessions" runat="server"
                     AutoGenerateColumns = "false"
                     DataSourceID = "odsSessions"
                     CssClass = "mGrid"
                     DataKeyNames = "ID,Date_Created,Date_Expired,User_Counts,Confirmed"
                     style = "margin: 0px auto;"
                     >
                     <Columns>
                        <asp:TemplateField HeaderText= "Идент. №">
                        <ItemTemplate>
                             <asp:Label ID="txtID" runat= "server" Text='<%# Bind("ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText= "Мрежа">
                        <ItemTemplate>
                            <%# Eval("Network.Name") %>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText= "Дата на създаване">
                        <ItemTemplate>
                            <%# Eval("Date_Created", "{0:dd-MMM-yyyy г.}") %>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText= "Валидност">
                        <ItemTemplate>
                            <%# Eval("Date_Expired", "{0:dd-MMM-yyyy г.}")%>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText= "Брой">
                        <ItemTemplate>
                            <%# Eval("User_Counts") %>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button runat="server"
                                     ID = "btnUpdate" CommandName= "Update" 
                                     Text = "Потвърди"
                                     CssClass="button"></asp:Button>

                                      <AjaxControlToolkit:ConfirmButtonExtender ID="cbeUpdate" runat="server"
                                        TargetControlID="btnUpdate" DisplayModalPopupID="mpeUpdate"
                            />
                                <AjaxControlToolkit:ModalPopupExtender
                                     ID= "mpeUpdate"
                                     runat = "server"
                                     CancelControlID = "cancel"
                                     PopupControlID = "popup"
                                     OkControlID = "btnOk"
                                     TargetControlID = "btnUpdate"
                                     BackgroundCssClass = "modalBackground"
                                />
                                <asp:Panel ID = "popup" runat="server" CssClass="modalPopup" style = "display:none; width: 400px; margin:0px auto;">
                                <br />
                                <p>Сигурни ли сте че искате за потвърдите плащането на лиценза?</p>
                                <p style="margin: 0px auto;">
                                    <asp:Button ID = "btnOk" runat="server" Text="Да"  CssClass = "button" />
                                    <asp:Button ID = "cancel" style="float:right;" runat="server" Text="Не" CssClass = "button" />
                                </p>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                     </Columns>
                     </asp:GridView>
                <asp:ObjectDataSource ID="odsSessions" runat="server"
                     DeleteMethod = "Delete"
                     SelectMethod = "SeleceAllNonConfirmedSessions"
                     UpdateMethod="ConfirmedUpdate"
                     TypeName="DataAccessLayer.LicensesDataAccess"
                     DataObjectTypeName="BussinessLayer.License"
                     >
                </asp:ObjectDataSource>
                <asp:ValidationSummary ID="vsUpdate" 
                    runat="server" style="text-align:left;"
                    ShowMessageBox="False" ShowSummary="True" DisplayMode="BulletList"
                    CssClass="validation"
                    />
                 <br />
                 <asp:PlaceHolder ID = "plhStatus" runat = "server">
                    <asp:Label ID = "lblStatus" runat = "server" CssClass = "warning" Visible = "false" ></asp:Label>
                 </asp:PlaceHolder>
                 <asp:PlaceHolder ID = "plhError" runat = "server" Visible = "false">
                    <asp:Label ID = "lblError" runat = "server" CssClass = "error"></asp:Label>
                 </asp:PlaceHolder>
                 <asp:PlaceHolder ID = "plhSuccess" runat = "server" Visible = "false">
                    <asp:Label ID = "lblSuccess" runat = "server" CssClass = "success"></asp:Label>
                 </asp:PlaceHolder>
            </div>
        </div>
</asp:Content>
