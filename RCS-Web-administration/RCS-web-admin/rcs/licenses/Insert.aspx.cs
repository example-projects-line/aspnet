﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessLayer;
using DataAccessLayer;

namespace RCS_web_admin.rcs.licenses
{
	public partial class Insert : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void btnBuy_Click(object sender, EventArgs e)
		{
			int usersCount			= Convert.ToInt32( txtUsersCount.Text );    
			if ( usersCount == 0 || usersCount < 0 )
			{
				lblError.Text = "Невалиден брой лицензи!";
                plhStatus.Visible = false;
                plhSuccess.Visible = false;
                plhError.Visible = true;
				return;
			}
			NetworksDataAccess	netsDa	= new NetworksDataAccess();
			LicensesDataAccess	licDA	= new LicensesDataAccess();

			License license			= new License();
			license.Date_Created	= DateTime.Now;
			license.Date_Expired	= DateTime.Now.AddMonths( 1 );	// License is valid only for 1 month.
			license.Network			= netsDa.selectNetworkByID( (int)Session["networkID"] );
			license.User_Counts		= usersCount;
			license.Confirmed		= false;

			try
			{
				licDA.Insert( license );

				lblSuccess.Text		= "Успешно поръчахте " + usersCount.ToString() + " потребителски лиценза!";
                plhSuccess.Visible	= true;
                plhStatus.Visible	= false;
                plhError.Visible	= false;

			}
			catch( Exception ex )
			{
				lblError.Text		= ex.Message;
                plhStatus.Visible	= false;
                plhSuccess.Visible	= false;
                plhError.Visible	= true;
			}

		}
	}
}