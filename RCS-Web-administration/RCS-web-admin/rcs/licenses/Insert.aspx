﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Insert.aspx.cs" Inherits="RCS_web_admin.rcs.licenses.Insert" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title></title>
	<link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/secondary.css" />
</head>
<body>
<div>
    <form id="frmMain" runat="server">
    <div align="center">
        <asp:TextBox ID="txtUsersCount" runat="server" ></asp:TextBox>
        <br /><br />
        <asp:Button ID="btnBuy" runat="server"  Text="Потвърди" CssClass="button" 
            onclick="btnBuy_Click" />
        <br /> <br />
        <br /> <br />
        <asp:PlaceHolder ID = "plhStatus" runat = "server">
        <asp:Label ID = "lblStatus" runat = "server" CssClass = "warning" Text = "Въведете брой лицензи, които желаете да закупите!" Visible = "true" ></asp:Label>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID = "plhError" runat = "server" Visible = "false">
        <asp:Label ID = "lblError" runat = "server" CssClass = "error"></asp:Label>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID = "plhSuccess" runat = "server" Visible = "false">
        <asp:Label ID = "lblSuccess" runat = "server" CssClass = "success"></asp:Label>
        </asp:PlaceHolder>
    </div>
    </form>
</div>
</body>
</html>
