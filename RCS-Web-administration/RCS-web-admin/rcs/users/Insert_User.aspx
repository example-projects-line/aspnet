﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Insert_User.aspx.cs" Inherits="RCS_web_admin.Insert_User" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <link rel="stylesheet" href="../../../assets/css/secondary.css" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class = "centered">
            <tr>
                <td>Потребителско име:</td>
                <td><asp:TextBox ID = "txtUsername" runat = "server"></asp:TextBox></td>
            </tr>
             <tr>
                <td>Парола:</td>
                <td><asp:TextBox ID = "txtPassword" runat = "server"
                                 TextMode = "Password"></asp:TextBox></td>
            </tr>
             <tr>
                <td>Име:</td>
                <td><asp:TextBox ID = "txtFirstname" runat = "server"></asp:TextBox></td>
            </tr>
              <tr>
                <td>Фамилия:</td>
                <td><asp:TextBox ID = "txtLastname" runat = "server"></asp:TextBox></td>
            </tr>
              <tr>
                <td>Имейл адрес:</td>
                <td><asp:TextBox ID = "txtEmail" runat = "server"></asp:TextBox></td>
            </tr>
              <tr>
                <td>Адрес:</td>
                <td><asp:TextBox ID = "txtAddress" runat = "server"></asp:TextBox></td>
            </tr>
              <tr>
                <td>Град:</td>
                <td><asp:TextBox ID = "txtCity" runat = "server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan = "2">
                    <br />
                  
                </td>
            </tr>
             <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnInsert" runat="server" Text="Добави" UseSubmitBehaviour="true" CssClass="button" OnClick="btnInsert_Click" />
                    </td>
                </tr>
                 <tr>
                    <td colspan="2">
                    <p>&nbsp;</p>
                    </td>
                </tr>
                  <tr>
                    <td colspan="2">
                        <asp:PlaceHolder ID="plhStatus" runat="server">
                            <asp:Label ID="lblStatus" runat="server" CssClass="warning" Text="Моля попълнете данните за новия потребител!" Visible="true"></asp:Label>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="plhError" runat="server"  Visible="false">
                            <asp:Label ID="lblError" runat="server" CssClass="error" Text=""></asp:Label>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="plhSuccess" runat="server" Visible="false">
                            <asp:Label ID="lblSuccess" runat="server"  CssClass="success" Text=""></asp:Label>
                        </asp:PlaceHolder>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
        </table>
        <asp:RegularExpressionValidator id="regvUsername" 
                    ControlToValidate="txtUsername"
                    Display="None"
                    ValidationExpression="^[a-zA-Zа-яА-Я0-9_-]{4,15}$"
                    ErrorMessage="Потребителското име може да съдържа само букви, цифри, и символите '_' или '-'. Минимална дължина 4 символа, максимална - 15."
                    runat="server"
                    />
                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server"
                    ErrorMessage="Потребителското име може да съдържа само букви, цифри, и символите '_' или '-'. Минимална дължина 4 символа, максимална - 15."
                    ControlToValidate="txtUsername"
                    Display="None"
                    />
            <asp:RegularExpressionValidator id="regvPassword" 
                    ControlToValidate="txtPassword"
                    Display="None"
                    ValidationExpression="^[a-zA-Z0-9#%^&<>?]{6,17}$"
                    ErrorMessage="Паролата може да съдържа само букви на латиница, цифри и символите '#', '%', '^', '&', '<', '>', '?'. Минимална дължина 6 символа, максимална - 17."
                    runat="server"
                    />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server"
                    ErrorMessage="Паролата може да съдържа само букви на латиница, цифри и символите '#', '%', '^', '&', '<', '>', '?'. Минимална дължина 6 символа, максимална - 17."
                    ControlToValidate="txtPassword"
                    Display="None"
                    />
            <asp:RegularExpressionValidator id="regvFirstName" 
                    ControlToValidate="txtFirstName"
                    Display="None"
                    ValidationExpression="^[a-zA-Zа-яА-Я]{1,255}$"
                    ErrorMessage="Невалидно име за потребител."
                    runat="server"
                    />
                    <asp:RequiredFieldValidator ID="rfvFirstName" runat="server"
                    ErrorMessage="Невалидно име за потребител."
                    ControlToValidate="txtFirstName"
                    Display="None"
                    />
            <asp:RegularExpressionValidator id="regvLastName" 
                    ControlToValidate="txtLastName"
                    Display="None"
                    ValidationExpression="^[a-zA-Zа-яА-Я]{1,255}$"
                    ErrorMessage="Невалидна фамилия за потребител."
                    runat="server"
                    />
                    <asp:RequiredFieldValidator ID="rfvLastName" runat="server"
                    ErrorMessage="Невалидна фамилия за потребител."
                    ControlToValidate="txtLastName"
                    Display="None"
                    />
            <asp:RegularExpressionValidator id="regvEmail" 
                    ControlToValidate="txtEmail"
                    Display="None"
                    ValidationExpression="^[a-zA-z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$"
                    ErrorMessage="Невалиден email адрес."
                    runat="server"
                    />
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server"
                    ErrorMessage="Невалиден email адрес."
                    ControlToValidate="txtEmail"
                    Display="None"
                    />
            <asp:RegularExpressionValidator id="regvAddress" 
                    ControlToValidate="txtAddress"
                    Display="None"
                    ValidationExpression="^[a-zA-Zа-яА-Я0-9\. ]{1,255}$"
                    ErrorMessage="Невалиден адрес на потребителя!"
                    runat="server"
                    />
                    <asp:RequiredFieldValidator ID="rfvAddress" runat="server"
                    ErrorMessage="Невалиден адрес на потребителя!"
                    ControlToValidate="txtAddress"
                    Display="None"
                    />
            <asp:RegularExpressionValidator id="regvCity" 
                    ControlToValidate="txtCity"
                    Display="None"
                    ValidationExpression="^[a-zA-Zа-яА-Я\ ]{1,255}$"
                    ErrorMessage="Невалидна име за град!"
                    runat="server"
                    />
                    <asp:RequiredFieldValidator ID="rfvCity" runat="server"
                    ErrorMessage="Невалидно име за град!"
                    ControlToValidate="txtCity"
                    Display="None"
                    />
            <%--<asp:RequiredFieldValidator ID="rfvRole" runat="server"
            ErrorMessage="Моля изберете правата за достъп на новия потребител."
            ControlToValidate="chblRoles"
            Display="None"
            />--%>
            <asp:ValidationSummary ID="vsUpdate" 
                runat="server"
                ShowMessageBox="False" ShowSummary="True" DisplayMode="BulletList"
                CssClass="validation"
                />
    </div>
    </form>
</body>
</html>
