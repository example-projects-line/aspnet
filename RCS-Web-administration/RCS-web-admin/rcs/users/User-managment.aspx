﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="User-managment.aspx.cs" Inherits="RCS_web_admin.User_managment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInfoStrip" runat="server">

    <h2><img src="../../assets/images/icons/user_32.png" alt="Login" />Управление на потребители</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMessageStrip" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContent" runat="server">
<asp:ScriptManager ID="scrManager" runat="server"></asp:ScriptManager>
    <div class="content-box">
	    <div class="content-box-header">
		    <h3>Потребители на мрежата</h3>
	    </div>
					
	    <div class="content-box-content">
            <asp:GridView ID="grvUsers" runat="server" CssClass="mGrid" AllowPaging="true"
                          DataSourceID = "odsUsers"
                          AutoGenerateColumns="false"
                          PagerStyle-CssClass="pgr"
                        AlternatingRowStyle-CssClass="alt"
                        DataKeyNames="ID,Username,Firstname,Lastname,Address,City" style="margin: 0px auto;"
                        >
                        <Columns>
                        <asp:TemplateField HeaderText="Идент. № " >
                          <ItemTemplate><%# Eval("ID") %></ItemTemplate>
                          </asp:TemplateField>
                           <asp:TemplateField HeaderText="Потребителско име" >
                          <ItemTemplate><%# Eval("Username") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Име на потребителя" >
                          <ItemTemplate><%# Eval("Firstname") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Фамилия на потребителя" >
                          <ItemTemplate><%# Eval("Lastname") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Адрес" >
                          <ItemTemplate><%# Eval("Address") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Град" >
                          <ItemTemplate><%# Eval("City") %></ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField>
                           <ItemTemplate>
                                <asp:ImageButton runat="server"
                                     ImageAlign="Middle"
                                     ImageUrl ="~/assets/images/delete.png"
                                     ID="btnDelete" CommandName = "Delete"
                                     Text= "Delete"></asp:ImageButton>
                           <AjaxControlToolkit:ConfirmButtonExtender ID="cbeDelete" runat="server"
                            TargetControlID="btnDelete" DisplayModalPopupID="mpeDelete"
                            />
                            <AjaxControlToolkit:ModalPopupExtender
                                ID="mpeDelete"
                                runat="server"
                                CancelControlID="cancel"
                                PopupControlID="popup"
                                OkControlID="btnOK"
                                TargetControlID="btnDelete"
                                BackgroundCssClass="modalBackground"
                                />
                                 <asp:Panel ID="popup" runat="server" CssClass="modalPopup" style="display:none;">
                                    <br />
                                <p>Сигурни ли сте, че искате да изтриете записа?</p>
                                <asp:Button ID="btnOK" runat="server" Text="Да" CssClass="button" />
                                <asp:Button ID="cancel" runat="server" Text="Отказ" CssClass="button" />
                            </asp:Panel>
                           </ItemTemplate>
                          </asp:TemplateField>
                           <asp:TemplateField>
                                <ItemTemplate>
                                    <input type="button" value="Промени" class="button" onclick="showPopUp( 'Update_User.aspx?id= <%# Eval("ID")%>', 450, 400, true );" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
            <asp:ObjectDataSource ID="odsUsers" runat="server" 
                        EnableViewState="False"
                        SelectMethod = "SelectAllNetworkUsers"
                        DeleteMethod = "Delete"
                        TypeName = "DataAccessLayer.UsersDataAccess"
                        DataObjectTypeName = "BussinessLayer.User"
                        >
                        <SelectParameters>
                            <asp:SessionParameter Name="networkID" SessionField="networkID" Type="Int32" DefaultValue="-1" />
                        </SelectParameters>
                        </asp:ObjectDataSource>
                    <div>
                        <br />
                        <input type = "button" value = "Добави нов" class = "button" onclick = "showPopUp('Insert_User.aspx', 450,400,true)"; />
                    </div>
        </div>
    </div>
</asp:Content>
