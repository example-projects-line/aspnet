﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Update_User.aspx.cs" Inherits="RCS_web_admin.Update_User" %>


<!DOCTYPE html>

<html>
<head>
  <link rel="stylesheet" href="../../../assets/css/secondary.css" type="text/css" />
    <title></title>
</head>
<body>
    <form id="frmUpdate" runat = "server">
        <div class="centered">
            <div align = "center">
            <br /> <br />
            <asp:DetailsView ID="dvUsers" runat = "server" Height = "50px" Width = "400px"
                 RowStyle-BorderStyle = "None"
                 AutoGenerateRows = "false"
                 CssClass = "detailsView" BorderStyle = "None" CellPadding = "3" GridLines = "None"
                 >
                    <Fields>
                        <asp:TemplateField HeaderText = "Идент. № " Visible = "true">
                            <ItemTemplate>
                                <asp:TextBox ID = "txtID" CssClass = "textbox" ReadOnly = "true" runat = "server" Text = '<%# Bind ( "ID" ) %>'></asp:TextBox>
                             </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText = "Потребителско име" Visible = "true">
                          <ItemTemplate>
                                <asp:TextBox ID = "txtUsername" CssClass = "textbox" runat = "server" Text = '<%# Bind ( "Username" ) %>'></asp:TextBox>
                                 <asp:RegularExpressionValidator id="regvUsername" 
                                        ControlToValidate="txtUsername"
                                        Display="None"
                                        ValidationExpression="^[a-zA-Zа-яА-Я0-9\ ]{1,255}$"
                                        ErrorMessage="Невалидно потребителско име  на потребителя!"
                                        runat="server"
                                        />
                                        <asp:RequiredFieldValidator ID="rfvUsername" runat="server"
                                        ErrorMessage="Невалидно потребителско име на потребителя!"
                                        ControlToValidate="txtUsername"
                                        Display="None"
                                        />
                             </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Име на потребителя">
                            <ItemTemplate>
                                <asp:TextBox ID = "txtFirstName" CssClass = "textbox" runat = "server" Text = '<%# Bind ( "FirstName" ) %>'></asp:TextBox>
                                 <asp:RegularExpressionValidator id="regvFirstName" 
                                        ControlToValidate="txtFirstName"
                                        Display="None"
                                        ValidationExpression="^[a-zA-Zа-яА-Я\ ]{1,255}$"
                                        ErrorMessage="Невалидно име на потребителя!"
                                        runat="server"
                                        />
                                        <asp:RequiredFieldValidator ID="rfvFirstName" runat="server"
                                        ErrorMessage="Невалидно име на потребителя!"
                                        ControlToValidate="txtFirstName"
                                        Display="None"
                                        />
                             </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText = "Фамилия на потребителя" Visible = "true">
                            <ItemTemplate>
                                <asp:TextBox ID = "txtLastName" CssClass = "textbox" runat = "server" Text = '<%# Bind ( "LastName" ) %>'></asp:TextBox>
                                 <asp:RegularExpressionValidator id="regvLastName" 
                                        ControlToValidate="txtLastName"
                                        Display="None"
                                        ValidationExpression="^[a-zA-Zа-яА-Я\ ]{1,255}$"
                                        ErrorMessage="Невалидна фамилия на потребителя!"
                                        runat="server"
                                        />
                                        <asp:RequiredFieldValidator ID="rfvLastName" runat="server"
                                        ErrorMessage="Невалидно име на потребителя!"
                                        ControlToValidate="txtLastName"
                                        Display="None"
                                        />
                             </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText = "Имейл на потребителя" Visible = "true">
                            <ItemTemplate>
                                <asp:TextBox ID = "txtEmail" CssClass = "textbox" runat = "server" Text = '<%# Bind ( "Email" ) %>'></asp:TextBox>
                                 <asp:RegularExpressionValidator id="regvEmail" 
                                        ControlToValidate="txtEmail"
                                        Display="None"
                                        ValidationExpression="^[a-zA-z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$"
                                        ErrorMessage="Невалиден имейл на потребителя!"
                                        runat="server"
                                        />
                                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server"
                                        ErrorMessage="Невалиден имейл на потребителя!"
                                        ControlToValidate="txtEmail"
                                        Display="None"
                                        />
                             </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText = "Адрес на потребителя" Visible = "true">
                            <ItemTemplate>
                                <asp:TextBox ID = "txtAddress" CssClass = "textbox" runat = "server" Text = '<%# Bind ( "Address" ) %>'></asp:TextBox>
                                 <asp:RegularExpressionValidator id="regvAddress" 
                                        ControlToValidate="txtAddress"
                                        Display="None"
                                        ValidationExpression="^[a-zA-Zа-яА-Я0-9\. ]{1,255}$"
                                        ErrorMessage="Невалиден адрес на потребителя!"
                                        runat="server"
                                        />
                                        <asp:RequiredFieldValidator ID="rfvAddress" runat="server"
                                        ErrorMessage="Невалиден адрес на потребителя!"
                                        ControlToValidate="txtAddress"
                                        Display="None"
                                        />
                             </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText = "Град" Visible = "true">
                            <ItemTemplate>
                                <asp:TextBox ID = "txtCity" CssClass = "textbox" runat = "server" Text = '<%# Bind ( "City" ) %>'></asp:TextBox>
                                 <asp:RegularExpressionValidator id="regvCity" 
                                        ControlToValidate="txtCity"
                                        Display="None"
                                        ValidationExpression="^[a-zA-Zа-яА-Я\ ]{1,255}$"
                                        ErrorMessage="Невалидна име за град!"
                                        runat="server"
                                        />
                                        <asp:RequiredFieldValidator ID="rfvCity" runat="server"
                                        ErrorMessage="Невалидно име за град!"
                                        ControlToValidate="txtCity"
                                        Display="None"
                                        />
                             </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <br />
                                <asp:Button ID="btnUpdate" runat="server" Text="Промени"  CssClass="button" OnClick="btnUpdate_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Fields>
                 </asp:DetailsView>
                 <asp:ValidationSummary ID="vsUpdate" 
                    runat="server" style="text-align:left;"
                    ShowMessageBox="False" ShowSummary="True" DisplayMode="BulletList"
                    CssClass="validation"
                    />
                 <br /> <br />
                 <asp:PlaceHolder ID = "plhStatus" runat = "server">
                    <asp:Label ID = "lblStatus" runat = "server" CssClass = "warning" Text = "Моля попълнете данните за новия потребител" Visible = "true" ></asp:Label>
                 </asp:PlaceHolder>
                 <asp:PlaceHolder ID = "plhError" runat = "server" Visible = "false">
                    <asp:Label ID = "lblError" runat = "server" CssClass = "error"></asp:Label>
                 </asp:PlaceHolder>
                 <asp:PlaceHolder ID = "plhSuccess" runat = "server" Visible = "false">
                    <asp:Label ID = "lblSuccess" runat = "server" CssClass = "success"></asp:Label>
                 </asp:PlaceHolder>
            </div>
        </div>
    </form>
</body>
</html>