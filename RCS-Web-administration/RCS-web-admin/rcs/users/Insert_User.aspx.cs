﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer;
using BussinessLayer;

namespace RCS_web_admin
{
    public partial class Insert_User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                UsersDataAccess usersDA		= new UsersDataAccess();
				LicensesDataAccess	licDA	= new LicensesDataAccess();
				int networkLicensesCount	= licDA.ListNetworkLicenses( (int)Session["networkID"] );
				int networkUsersCount		= usersDA.SelectAllNetworkUsers( (int)Session["networkID"] ).Count;

				if( networkLicensesCount <= networkUsersCount )
				{
					lblError.Text = "Недостатъчен брой лицензи!";

					plhStatus.Visible = false;
					plhSuccess.Visible = false;
					plhError.Visible = true;
					return;
				}
                
				BussinessLayer.User user = new BussinessLayer.User();
                user.Username = txtUsername.Text;
                user.Password = txtPassword.Text;
                user.Firstname = txtFirstname.Text;
                user.Lastname = txtLastname.Text;
                user.Email = txtEmail.Text;
                user.Address = txtAddress.Text;
                user.City = txtCity.Text;

				NetworksDataAccess	netsDA	= new NetworksDataAccess();
				Network	net = netsDA.selectNetworkByID( (int)Session["networkID"] );
				user.Network	= net;
                usersDA.Insert(user);

                lblSuccess.Text = "Потребителят беше добавен успешно !";
                plhSuccess.Visible = true;

                plhStatus.Visible = false;
                plhError.Visible = false;
            }
            catch (Exception ex)
            {
                if(ex.Message.Contains("UNIQUE KEY") )
                    lblError.Text = "Потребителското име/емайл адрес вече съществува";
                else
                    lblError.Text = ex.Message;

                plhStatus.Visible = false;
                plhSuccess.Visible = false;
                plhError.Visible = true;
            }
        }
    }
}
