﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer;
using BussinessLayer;


namespace RCS_web_admin
{
    public partial class Update_User : System.Web.UI.Page
    {
        private User m_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (! IsPostBack)
                load();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            TextBox txtID = (TextBox)Page.Form.FindControl("dvUsers:txtID");
            TextBox txtUsername = (TextBox)Page.Form.FindControl("dvUsers:txtUsername");
            TextBox txtFirstname = (TextBox)Page.Form.FindControl("dvUsers:txtFirstName");
            TextBox txtLastname = (TextBox)Page.Form.FindControl("dvUsers:txtLastName");
            TextBox txtEmail = (TextBox)Page.Form.FindControl("dvUsers:txtEmail");
            TextBox txtAddress = (TextBox)Page.Form.FindControl("dvUsers:txtAddress");
            TextBox txtCity = (TextBox)Page.Form.FindControl("dvUsers:txtCity");


            User user = new User();

            user.ID = Convert.ToInt32(txtID.Text);
            user.Username = txtUsername.Text;
            user.Firstname = txtFirstname.Text;
            user.Lastname = txtLastname.Text;
            user.Email = txtEmail.Text;
            user.Address = txtAddress.Text;
            user.City = txtCity.Text;

            try
            {
                UsersDataAccess usersDA = new UsersDataAccess();

                usersDA.Update(user);

                lblSuccess.Text = "Данните на клиента бяха успешно променени!";
                plhSuccess.Visible = true;

                plhStatus.Visible = false;
                plhError.Visible = false;

            }
            catch (Exception ex)
            {
                lblError.Text		= ex.Message;

                plhStatus.Visible	= false;
                plhSuccess.Visible	= false;
                plhError.Visible	= true;

            }
        }

        private void load()
        {
            int userID = -1;

            if ( Request.Params["id"] != null )
            userID  = Convert.ToInt32(Request.Params["id"] ) ;

            if(userID != -1)
            {
                UsersDataAccess userDA = new UsersDataAccess();
                m_user = userDA.selectUserByID ( userID ) ;
                List<User> users = new List<User>();

                if(m_user != null)
                {
                    users.Add ( m_user );

                    dvUsers.DataSource = users;
                    dvUsers.DataBind();
                }

            }
        }
    }
}