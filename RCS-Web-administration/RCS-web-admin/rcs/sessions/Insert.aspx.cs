﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer;

namespace RCS_web_admin.rcs.sessions
{
	public partial class Insert : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		private void	insert()
		{
			int	userID				= Convert.ToInt32( Request.Params["user_id"] );
			UsersDataAccess usersDA	= new UsersDataAccess();
			SessionsDataAccess	sessionsDA	= new SessionsDataAccess();

			BussinessLayer.Session	session	= new BussinessLayer.Session();
			session.HostIP		= Request.Params["host_ip"];
			session.RemoteIP	= Request.Params["remote_ip"];
			session.User		= usersDA.selectUserByID( userID );

			sessionsDA.Insert( session );
		}
	}
}