﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ViewNetworkSessions.aspx.cs" Inherits="RCS_web_admin.rcs.sessions.ViewNetworkSessions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInfoStrip" runat="server">
    <h2><img src="../../assets/images/icons/user_32.png" alt="" />Сесии</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMessageStrip" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContent" runat="server">
 
<asp:ScriptManager ID="scrManager" runat="server"></asp:ScriptManager>
        <div class="content-box">
	    <div class="content-box-header">
		    <h3>Регистрирани сесии</h3>
	    </div>
					
	    <div class="content-box-content">
            <asp:GridView ID="grvSessions" runat="server"
                AutoGenerateColumns = "false"
                DataSourceID="odsSessions"
                CssClass="mGrid"
                DataKeyNames="ID, HostIP, RemoteIP, Duration" style="margin: 0px auto;"
            >
                <Columns>
                     <asp:TemplateField HeaderText="Идент. №">
                    <ItemTemplate><%# Eval("ID") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Потребител">
                    <ItemTemplate><%# Eval("User.Username")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Host IP">
                    <ItemTemplate><%# Eval("HostIP")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Remote IP">
                    <ItemTemplate><%# Eval("RemoteIP")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Продължителност">
                    <ItemTemplate><%# Eval("Duration")%></ItemTemplate>
                </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsSessions" runat="server"
                SelectMethod="SelectAllNetworkSessions"
                TypeName="DataAccessLayer.SessionsDataAccess"
                DataObjectTypeName="BussinessLayer.Session"
            >
            <SelectParameters>
                <asp:SessionParameter Name="netID" SessionField="networkID" Type="Int32" DefaultValue="-1" />
            </SelectParameters>
            </asp:ObjectDataSource>
        </div>
    </div>
</asp:Content>
