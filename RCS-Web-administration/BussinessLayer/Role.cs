﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BussinessLayer
{
   public class Role
    {
        private int m_ID;
        private string m_name;

        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
    }
}
