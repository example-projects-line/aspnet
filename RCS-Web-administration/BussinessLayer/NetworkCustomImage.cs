﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BussinessLayer
{
	public	class NetworkCustomImage
	{
		private string	m_name;
		private string	m_extension;
		private int		m_size;
		private byte[]	m_content;

		public byte[] Content
		{
			get { return m_content; }
			set { m_content = value; }
		}

		public int Size
		{
			get { return m_size; }
			set { m_size = value; }
		}

		public string Extension
		{
			get { return m_extension; }
			set { m_extension = value; }
		}

		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}
	}
}
