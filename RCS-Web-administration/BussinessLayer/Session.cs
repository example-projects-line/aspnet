﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BussinessLayer
{
	public	class Session
	{
		private int m_ID;
		private User m_user;
		private string m_hostIP;
		private string m_remoteIP;
		private TimeSpan m_duration;

		public TimeSpan Duration
		{
			get { return m_duration; }
			set { m_duration = value; }
		}

		public string RemoteIP
		{
			get { return m_remoteIP; }
			set { m_remoteIP = value; }
		}

		public string HostIP
		{
			get { return m_hostIP; }
			set { m_hostIP = value; }
		}

		public User User
		{
			get { return m_user; }
			set { m_user = value; }
		}

		public int ID
		{
			get { return m_ID; }
			set { m_ID = value; }
		}
	}
}
