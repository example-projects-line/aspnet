﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BussinessLayer
{
	public	class Network
	{
		private int		m_ID;
		private string m_name;
		private string m_password;
		private string m_firstName;
		private string m_lastName;
		private string m_address;
		private string m_city;
		private string m_email;

		public string Password
		{
			get { return m_password; }
			set { m_password = value; }
		}

		public string Email
		{
			get { return m_email; }
			set { m_email = value; }
		}

		public string City
		{
			get { return m_city; }
			set { m_city = value; }
		}

		public string Address
		{
			get { return m_address; }
			set { m_address = value; }
		}

		public string LastName
		{
			get { return m_lastName; }
			set { m_lastName = value; }
		}

		public string FirstName
		{
			get { return m_firstName; }
			set { m_firstName = value; }
		}

		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}

		public int ID
		{
			get { return m_ID; }
			set { m_ID = value; }
		}
	}
}
