﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BussinessLayer
{
    public class License
    {
        private int m_ID;
        private Network m_network;
        private DateTime m_Date_Created;
        private DateTime m_Date_Expired;
        private int m_User_Counts;
        private bool m_Confirmed;

        #region Properties
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public Network Network
        {
            get { return m_network; }
            set { m_network = value; }
        }

        public DateTime Date_Created
        {
            get { return m_Date_Created; }
            set { m_Date_Created = value; }

        }

        public DateTime Date_Expired
        {
            get { return m_Date_Expired; }
            set { m_Date_Expired = value; }

        }

        public int User_Counts
        {
            get { return m_User_Counts; }
            set { m_User_Counts = value; }
        }

        public bool Confirmed
        {
            get { return m_Confirmed; }
            set { m_Confirmed = value; }

        }





        #endregion
    }
}
