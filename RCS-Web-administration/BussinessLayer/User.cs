﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BussinessLayer
{
    public class User
    {
        private int m_ID;
        private string m_username;
        private string m_password;
        private string m_firstname;
        private string m_lastname;
        private string m_email;
        private string m_address;
        private string m_city;
        private List<Role> m_roles;
		private Network m_network;

		public Network Network
		{
			get { return m_network; }
			set { m_network = value; }
		}

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public string Username
        {
            get { return m_username; }
            set { m_username = value; }
        }

        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }

        public string Firstname
        {
            get { return m_firstname; }
            set { m_firstname = value; }
        }

        public string Lastname
        {
            get { return m_lastname; }
            set { m_lastname = value; }
        }

        public string Email
        {
            get { return m_email; }
            set { m_email = value; }
        }

        public string Address
        {
            get { return m_address; }
            set { m_address = value; }
        }

        public string City
        {
            get { return m_city; }
            set { m_city = value; }
        }

        public List<Role> Roles
        {
            get { return m_roles; }
            set { m_roles = value; }
        }
    }
}
