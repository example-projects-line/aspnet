﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BussinessLayer;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DataAccessLayer
{
    public class LicensesDataAccess
    {
        #region Insert
        /// <summary>
        /// Inserts a new License in the database.
        /// </summary>
       
        public void Insert(License license)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = @"INSERT INTO Licenses(Network_ID,Date_Created,Date_Expire,Users_Count,Confirmed)
                                               VALUES (@Network_ID,@Date_Created,@Date_Expire,@Users_Count,@Confirmed)";

            command.Parameters.Add(new SqlParameter("@Network_ID", license.Network.ID));
            command.Parameters.Add(new SqlParameter("@Date_Created", license.Date_Created));
            command.Parameters.Add(new SqlParameter("@Date_Expire", license.Date_Expired));
            command.Parameters.Add(new SqlParameter("@Users_Count", license.User_Counts));
            command.Parameters.Add(new SqlParameter("@Confirmed", license.Confirmed ? 1 : 0));


            connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw ex;
            }

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the information about a supplier in the database
        /// </summary>
        /// <param name="Supplier">The supplier to be updated.</param>
        public void Update(License license)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = @"UPDATE Licenses SET ID=@ID,Network_ID=@Network_ID,Date_Created=@Date_Created,Date_Expire=@Date_Expire,
                                                        Users_Count=@Users_Count,Confirmed=@Confirmed WHERE ID=@ID";

            command.Parameters.Add(new SqlParameter("@ID", license.ID));
            command.Parameters.Add(new SqlParameter("@Network_ID", license.Network.ID));
            command.Parameters.Add(new SqlParameter("@Date_Created", license.Date_Created));
            command.Parameters.Add(new SqlParameter("@Date_Expire", license.Date_Expired));
            command.Parameters.Add(new SqlParameter("@Users_Count", license.User_Counts));
            command.Parameters.Add(new SqlParameter("@Confirmed", license.Confirmed ? 1 : 0));

            connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw ex;
            }

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }

        #endregion

        public void ConfirmedUpdate(License license)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = @"UPDATE Licenses SET Confirmed=1 WHERE ID=@ID";

            command.Parameters.Add(new SqlParameter("@ID", license.ID));

            connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw ex;
            }

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }

        #region Delete
        /// <summary>
        /// Delete the information about a supplier in the database
        /// </summary>
        /// <param name="Supplier">The supplier to be deleted.</param>


        public void Delete(License license)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = @"DELETE FROM Licenses WHERE ID=@ID";

            command.Parameters.Add(new SqlParameter("@ID", license.ID));

            connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw ex;

            }

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }

        #endregion

        #region Select License By ID
        /// <summary>
        /// Returns an 'Supplier' bussiness object by a given ID.
        /// </summary>
        /// <param name="ID">ID of the supplier.</param>
        /// <returns></returns>
        public License selectLicenseByID(int ID)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "SELECT * FROM Licenses WHERE ID = @ID";
            command.Parameters.Add(new SqlParameter("@ID", ID));

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
			NetworksDataAccess	netsDA	= new NetworksDataAccess();

            if (reader.Read())
            {
                License license = new License();

                license.ID = (int)reader["ID"];
                license.Network = netsDA.selectNetworkByID( (int)reader["Network_ID"] );
                license.Date_Created = (DateTime)reader["Date_Created"];
                license.Date_Expired = (DateTime)reader["Date_Expire"];
                license.User_Counts = (int)reader["User_Counts"];
                license.Confirmed = (int)reader["Confirmed"] == 1 ? true : false;
                
                

                connection.Close();
                reader.Close();

                return license;
            }

            connection.Close();
            reader.Close();

            return null;
        }
        #endregion

        #region Select All License
        /// <summary>
        /// Selects all suppliers registered in the database.
        /// </summary>
        public List<License> SelectAllLicenses()
        {
            List<License> lstLicenses = new List<License>();
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "SELECT * FROM Licenses";

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
			NetworksDataAccess	netsDA	= new NetworksDataAccess();

            while (reader.Read())
            {
                License license = new License();

                license.ID				= (int)reader["ID"];
                license.Network			= netsDA.selectNetworkByID( (int)reader["Network_ID"] );
                license.Date_Created	= (DateTime)reader["Date_Created"];
                license.Date_Expired	= (DateTime)reader["Date_Expire"];
                license.User_Counts		= (int)reader["Users_Count"];
                license.Confirmed		= (int)reader["Confirmed"] == 1 ? true : false;

                lstLicenses.Add(license);
            }

            connection.Close();
            reader.Close();

            return lstLicenses;
        }
        #endregion

        #region SeleceAllNonConfirmedSessions
        /// <summary>
        /// Selects all suppliers registered in the database.
        /// </summary>
         public List<License> SeleceAllNonConfirmedSessions()
        {
            List<License> lstLicenses = new List<License>();
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "SELECT * FROM Licenses WHERE Confirmed = 0";

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
			NetworksDataAccess	netsDA	= new NetworksDataAccess();

            while (reader.Read())
            {
                License license = new License();

                license.ID				= (int)reader["ID"];
                license.Network			= netsDA.selectNetworkByID( (int)reader["Network_ID"] );
                license.Date_Created	= (DateTime)reader["Date_Created"];
                license.Date_Expired	= (DateTime)reader["Date_Expire"];
                license.User_Counts		= (int)reader["Users_Count"];
                license.Confirmed		= (int)reader["Confirmed"] == 1 ? true : false;

                lstLicenses.Add(license);
            }

            connection.Close();
            reader.Close();

            return lstLicenses;
        }
        #endregion

		#region Network licenses

		public int ListNetworkLicenses(int networkID)
        {
            string connString	= ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn	= new SqlConnection( connString );
            SqlCommand command	= new SqlCommand();
            command.Connection	= conn;
            command.CommandText	= "SELECT COUNT(Users_Count) AS CNT FROM Licenses WHERE ID=@ID AND Confirmed = 1";

            command.Parameters.Add(new SqlParameter("@ID", networkID ));

            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            if( reader.Read() )
            {
                int count = (int)reader["CNT"];
                conn.Close();
                reader.Close();
                return count;
            }

            return -1;
        }

		#endregion
	}
}
