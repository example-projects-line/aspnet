﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BussinessLayer;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DataAccessLayer
{
	public	class NetworksDataAccess
	{
		public Network selectNetworkByID( int ID )
        {
            string connString	= ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn	= new SqlConnection(connString);
            SqlCommand command	= new SqlCommand();
            command.Connection	= conn;
            command.CommandText = "SELECT * FROM Networks WHERE ID = @Network_ID";
            command.Parameters.Add( new SqlParameter("@Network_ID", ID) );

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();

            if(reader.Read() )
            {
                Network network			= new Network();

                network.ID			= (int)reader["ID"];
                network.Name		= (string)reader["Name"];
                network.Password	= (string)reader["Password"];
                network.FirstName	= (string)reader["First_Name"];
                network.LastName	= (string)reader["Last_Name"];
                network.Email		= (string)reader["Email"];
                network.City		= (string)reader["City"];
                network.Address		= (string)reader["Address"];

                conn.Close();
                reader.Close();

                return network;
            }

            conn.Close();
            reader.Close();

            return null;
        }

		 public List<Network> SelectAllNetworks()
        {
            List<Network> listNets = new List<Network>();
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = "SELECT * FROM Networks";

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Network net = new Network();
                net.ID = (int)reader["ID"];
                net.Name = (string)reader["Name"];
                net.Password = (string)reader["Password"];
                net.FirstName = (string)reader["First_Name"];
                net.LastName = (string)reader["Last_Name"];
                net.Email = (string)reader["Email"];
                net.Address = (string)reader["Address"];
                net.City = (string)reader["City"];

                listNets.Add(net);
            }
            conn.Close();
            reader.Close();

            return listNets;
        }

		public int Insert( Network network )
        {
            string connString	= ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn	= new SqlConnection(connString);
            SqlCommand command	= new SqlCommand();
            command.Connection	= conn;
            command.CommandText = @"INSERT INTO Networks(Name, Password, First_Name, Last_Name, Email, Address, City)
                                    VALUES (@Name, @Password, @First_Name, @Last_Name, @Email, @Address, @City);
											SELECT CAST(scope_identity() AS int);";

          
             command.Parameters.AddWithValue("@Name",		network.Name);
             command.Parameters.AddWithValue("@Password",	network.Password);
             command.Parameters.AddWithValue("@First_Name", network.FirstName);
             command.Parameters.AddWithValue("@Last_Name",	network.LastName);
             command.Parameters.AddWithValue("@Email",		network.Email);
             command.Parameters.AddWithValue("@City",		network.City);
             command.Parameters.AddWithValue("@Address",	network.Address);
             conn.Open();

            try
            {
                network.ID	= (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                conn.Close();
				throw ex;
            }

            if (conn.State == ConnectionState.Open)
                conn.Close();

			return network.ID;
        }

        public void Update( Network network )
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString; ;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = @"UPDATE Networks SET Name = @Name,
                                    First_Name = @First_Name, Last_Name = @Last_Name, Email = @Email,
                                    City = @City, Address = @Address WHERE ID = @ID";


            command.Parameters.Add(new SqlParameter("@ID",			network.ID));
            command.Parameters.Add(new SqlParameter("@Name",		network.Name));
            command.Parameters.Add(new SqlParameter("@First_Name",	network.FirstName));
            command.Parameters.Add(new SqlParameter("@Last_Name",	network.LastName));
            command.Parameters.Add(new SqlParameter("@Email",		network.Email));
            command.Parameters.Add(new SqlParameter("@City",		network.City));
            command.Parameters.Add(new SqlParameter("@Address",		network.Address));

            conn.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            if (conn.State == ConnectionState.Open)
                conn.Close();
        }

		public void Delete(Network network)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = @"DELETE FROM Networks WHERE ID= @ID";

            command.Parameters.Add(new SqlParameter("@ID", network.ID));

            conn.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch ( Exception ex)
            {
                conn.Close();
				throw ex;
            }
            if(conn.State == ConnectionState.Open)
                conn.Close();
        }
	}
}
