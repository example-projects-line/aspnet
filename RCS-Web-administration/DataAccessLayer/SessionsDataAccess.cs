﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BussinessLayer;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DataAccessLayer
{
	public class SessionsDataAccess
	{
		public void Insert(Session session)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = @"INSERT INTO Licenses(User_ID,Host_IP,Remote_IP,Duration)
                                               VALUES (@User_ID,@Host_IP,@Remote_IP,@Duration)";

            command.Parameters.Add(new SqlParameter("@User_ID", session.User.ID));
            command.Parameters.Add(new SqlParameter("@Host_IP", session.HostIP));
            command.Parameters.Add(new SqlParameter("@Remote_IP", session.RemoteIP));
            command.Parameters.Add(new SqlParameter("@Duration", session.Duration));

            connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw ex;
            }

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }

		public List<Session> SelectAllNetworkSessions( int netID )
        {
            List<Session> lstSessions = new List<Session>();
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "SELECT * FROM Sessions WHERE User_ID in ( SELECT ID FROM Users WHERE Network_ID = @Network_ID )";
			command.Parameters.Add( new SqlParameter("@Network_ID", netID) );

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
			UsersDataAccess	usersDA	= new UsersDataAccess();

            while (reader.Read())
            {
                Session session = new Session();

                session.ID				= (int)reader["ID"];
                session.User			= usersDA.selectUserByID( (int)reader["User_ID"] );
                session.HostIP			= (string)reader["Host_IP"];
                session.RemoteIP		= (string)reader["Remote_IP"];
                session.Duration		= (TimeSpan)reader["Duration"];

                lstSessions.Add(session);
            }

            connection.Close();
            reader.Close();

            return lstSessions;
        }
	}
}
