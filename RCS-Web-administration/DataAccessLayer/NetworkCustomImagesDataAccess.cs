﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using BussinessLayer;
using System.Data;

namespace DataAccessLayer
{
	public	class NetworkCustomImagesDataAccess
	{
		public void	Insert( NetworkCustomImage image, int networkID )
		{
			string connString			= ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
			SqlConnection connection	= new SqlConnection(connString);

			SqlCommand command	= new SqlCommand();
			command.Connection	= connection;
			command.CommandText = @"INSERT INTO NetworkCustomImages( Network_ID, Name, format, content )
												VALUES (@Network_ID, @Name, @format, @content )";

			command.Parameters.Add(  new SqlParameter("@Network_ID",		networkID)		);
			command.Parameters.Add(  new SqlParameter("@Name",				image.Name)		);
			command.Parameters.Add(  new SqlParameter("@format",			image.Extension));
			command.Parameters.Add(  new SqlParameter("@content",			image.Content)	);

			connection.Open();

			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				connection.Close();

				throw ex;
			}

			if ( connection.State == ConnectionState.Open )
				connection.Close();
		}

		public	NetworkCustomImage	SelectNetworkImage( int networkID )
		{
			string connString		= ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
			SqlConnection connection= new SqlConnection(connString);
			SqlCommand command		= new SqlCommand();
			command.Connection		= connection;
			command.CommandText		= "SELECT * FROM NetworkCustomImages WHERE Network_ID=@ID";
			command.Parameters.Add( new SqlParameter("@ID", networkID) );

			connection.Open();

			SqlDataReader			reader	= command.ExecuteReader();
			NetworkCustomImage		image	= new NetworkCustomImage();
			
			if( reader.Read() )
			{
				image.Extension		= (string)reader["format"];
				image.Name			= (string)reader["Name"];
				image.Content		= (byte[])reader["content"];
				connection.Close();
				reader.Close();

				return image;
			}

			return null;
		}

		public	void	Update( NetworkCustomImage image, int networkID )
		{
			string connString			= ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
			SqlConnection connection	= new SqlConnection(connString);

			SqlCommand command	= new SqlCommand();
			command.Connection	= connection;
			command.CommandText = @"UPDATE NetworkCustomImages SET Name=@Name, format=@format, content=@content
									WHERE Network_ID=@ID";

			command.Parameters.Add(  new SqlParameter("@ID",		networkID)		);
			command.Parameters.Add(  new SqlParameter("@Name",		image.Name)		);
			command.Parameters.Add(  new SqlParameter("@format",	image.Extension));
			command.Parameters.Add(  new SqlParameter("@content",	image.Content)	);

			connection.Open();

			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				connection.Close();

				throw ex;
			}

			if ( connection.State == ConnectionState.Open )
				connection.Close();
		}

		public	void	Delete( int networkID )
		{
			string connString			= ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
			SqlConnection connection	= new SqlConnection(connString);
			SqlCommand command			= new SqlCommand();
			command.Connection			= connection;
			command.CommandText			= @"DELETE FROM NetworkCustomImages WHERE Network_ID=@ID";
		   
			command.Parameters.Add( new SqlParameter("@ID", networkID) );
			connection.Open();

			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				connection.Close();

				throw ex;
			}

			if (connection.State == ConnectionState.Open)
				connection.Close();
		}
	}
}
