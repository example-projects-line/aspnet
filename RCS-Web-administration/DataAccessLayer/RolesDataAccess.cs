﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BussinessLayer;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace DataAccessLayer
{
   public class RolesDataAccess
    {
        public Role selectRoleByID(int roleID)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = "SELECT * FROM Roles WHERE ID = @RoleID";
            command.Parameters.Add(new SqlParameter("@RoleID", roleID));

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();

            if(reader.Read())
            {
                Role role = new Role();
                role.ID = (int)reader["ID"];
                role.Name = (string)reader["Name"];

                conn.Close();
                reader.Close();

                return role;
            }

            conn.Close();
            reader.Close();

            return null;
        }

        public List<Role> selectUserRoles(int userID)
        {
            List<Role> roles = null;

            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = "SELECT * FROM UserRoles WHERE User_ID = @UserID";
            command.Parameters.Add(new SqlParameter("@UserID", userID));

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows) 
            {
                roles = new List<Role>();
                while(reader.Read())
                {
                    int roleID = (int)reader["Role_ID"];
                    Role role = selectRoleByID(roleID);
                    
                    if(role != null)
                    {
                        roles.Add(role);
                    }
                }
            }

            conn.Close();
            reader.Close();

            return roles;
        }

        public void InsertUserToRole(User user, Role role)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = @"INSERT INTO UserRoles(User_ID, Role_ID) VALUES (@User_ID, @Role_ID)";

            command.Parameters.Add(new SqlParameter("@User_ID", user.ID));
            command.Parameters.Add(new SqlParameter("@Role_ID", role.ID));

            connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                throw ex;
            }

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }

        public List<Role> selectAllRoles()
        {
            List<Role> roles = null;
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = "SELECT * FROM Roles";

            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                roles = new List<Role>();
                while (reader.Read())
                {
                    Role role = new Role();
                    role.ID = (int)reader["ID"];
                    role.Name = (string)reader["Name"];

                    roles.Add(role);
                }
            }

            conn.Close();
            reader.Close();

            return roles;

        }
    }
}
