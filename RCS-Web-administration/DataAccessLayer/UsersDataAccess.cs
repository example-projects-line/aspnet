﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BussinessLayer;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace DataAccessLayer
{
    public class UsersDataAccess
    {
        public User selectUserByID(int ID)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = "SELECT * FROM Users WHERE ID = @User_ID";
            command.Parameters.Add(new SqlParameter("@User_ID", ID));

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();
			NetworksDataAccess	netsDA	= new NetworksDataAccess();

            if(reader.Read() )
            {
                User user = new User();
                RolesDataAccess rolesDA = new RolesDataAccess();

                user.ID = (int)reader["ID"];
				user.Network	= netsDA.selectNetworkByID( (int)reader["Network_ID"] );
                user.Username = (string)reader["Username"];
                user.Password = (string)reader["Password"];
                user.Firstname = (string)reader["First_Name"];
                user.Lastname = (string)reader["Last_Name"];
                user.Email = (string)reader["Email"];
                user.City = (string)reader["City"];
                user.Address = (string)reader["Address"];

                conn.Close();
                reader.Close();

                return user;
            }

            conn.Close();
            reader.Close();

            return null;
        }

        public User selectUserByUsername(string username)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = "SELECT * FROM Users WHERE Username = @Username";
            command.Parameters.Add(new SqlParameter("@Username", username));

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();
			NetworksDataAccess	netsDA	= new NetworksDataAccess();

            if (reader.Read())
            {
                User user = new User();
                RolesDataAccess rolesDA = new RolesDataAccess();

                user.ID = (int)reader["ID"];
				user.Network	= netsDA.selectNetworkByID( (int)reader["Network_ID"] );
                user.Username = (string)reader["Username"];
                user.Firstname = (string)reader["First_Name"];
                user.Lastname = (string)reader["Last_Name"];
                user.Password = (string)reader["Password"];
                user.Email = (string)reader["Email"];
                user.City = (string)reader["City"];
                user.Address = (string)reader["Address"];
                user.Roles = rolesDA.selectUserRoles(user.ID);

                conn.Close();
                reader.Close();

                return user;
            }

            conn.Close();
            reader.Close();

            return null;
        }

        public bool validateLogin(string username, string password)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = "SELECT * FROM Users WHERE Username = @Username AND Password = @Password";
            command.Parameters.Add(new SqlParameter("@Username", username));
            command.Parameters.Add(new SqlParameter("@Password", password));

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                conn.Close();
                reader.Close();
                return true;
            }

            conn.Close();
            reader.Close();

            return false;
        }

        public List<User> SelectAllUsers()
        {
            List<User> listUsers = new List<User>();
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = "SELECT * FROM Users";

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();
			NetworksDataAccess	netsDA	= new NetworksDataAccess();

            while (reader.Read())
            {
                User user = new User();
                RolesDataAccess rolesDA = new RolesDataAccess();

                user.ID = (int)reader["ID"];
				user.Network	= netsDA.selectNetworkByID( (int)reader["Network_ID"] );
                user.Username = (string)reader["Username"];
                user.Password = (string)reader["Password"];
                user.Firstname = (string)reader["First_Name"];
                user.Lastname = (string)reader["Last_Name"];
                user.Email = (string)reader["Email"];
                user.Address = (string)reader["Address"];
                user.City = (string)reader["City"];
                user.Roles = rolesDA.selectUserRoles(user.ID);

                listUsers.Add(user);
            }
            conn.Close();
            reader.Close();

            return listUsers;
        }

		public List<User> SelectAllNetworkUsers( int networkID )
        {
            List<User> listUsers = new List<User>();
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = "SELECT * FROM Users WHERE Network_ID = @Network_ID";
			command.Parameters.Add( new SqlParameter("@Network_ID", networkID) );

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();
			NetworksDataAccess	netsDA	= new NetworksDataAccess();

            while (reader.Read())
            {
                User user = new User();
                RolesDataAccess rolesDA = new RolesDataAccess();

                user.ID = (int)reader["ID"];
				user.Network	= netsDA.selectNetworkByID( (int)reader["Network_ID"] );
                user.Username = (string)reader["Username"];
                user.Password = (string)reader["Password"];
                user.Firstname = (string)reader["First_Name"];
                user.Lastname = (string)reader["Last_Name"];
                user.Email = (string)reader["Email"];
                user.Address = (string)reader["Address"];
                user.City = (string)reader["City"];
                user.Roles = rolesDA.selectUserRoles(user.ID);

                listUsers.Add(user);
            }
            conn.Close();
            reader.Close();

            return listUsers;
        }

        public void Insert(User user)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = @"INSERT INTO Users(Username, Network_ID, Password, First_Name, Last_Name, Email, Address, City)
                                    VALUES (@Username, @Network_ID, @Password, @First_Name, @Last_Name, @Email, @Address, @City);
											SELECT CAST(scope_identity() AS int);";

			command.Parameters.Add( new SqlParameter("@Network_ID", user.Network.ID) );
            command.Parameters.AddWithValue("@Username", user.Username);
            command.Parameters.AddWithValue("@Password", user.Password);
            command.Parameters.AddWithValue("@First_Name", user.Firstname);
            command.Parameters.AddWithValue("@Last_Name", user.Lastname);
            command.Parameters.AddWithValue("@Email", user.Email);
            command.Parameters.AddWithValue("@City", user.City);
            command.Parameters.AddWithValue("@Address", user.Address);
            conn.Open();

          

            try
            {
                user.ID	= (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                conn.Close();
				throw ex;
            }

            if (conn.State == ConnectionState.Open)
                conn.Close();
			
			if( user.Roles != null )
			{
				RolesDataAccess	rolesDA	= new RolesDataAccess();

				for( int i = 0; i < user.Roles.Count; i++ )
					rolesDA.InsertUserToRole( user, user.Roles[i] );
			}
        }

        public void Update(User user)
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString; ;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = @"UPDATE Users SET Username = @Username,
                                    First_Name = @First_Name, Last_Name = @Last_Name, Email = @Email,
                                    City = @City, Address = @Address WHERE ID = @ID";


            command.Parameters.Add(new SqlParameter("@ID", user.ID));
            command.Parameters.Add(new SqlParameter("@Username", user.Username));
            command.Parameters.Add(new SqlParameter("@First_Name", user.Firstname));
            command.Parameters.Add(new SqlParameter("@Last_Name", user.Lastname));
            command.Parameters.Add(new SqlParameter("@Email", user.Email));
            command.Parameters.Add(new SqlParameter("@City", user.City));
            command.Parameters.Add(new SqlParameter("@Address", user.Address));

            conn.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            if (conn.State == ConnectionState.Open)
                conn.Close();
        }

        public void Delete(User user) // here we need to talk about what would be the logic for deleting a user
        {
            string connString = ConfigurationManager.ConnectionStrings["DBConnString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = @"DELETE FROM Users WHERE ID= @ID";

            command.Parameters.Add(new SqlParameter("@ID", user.ID));

            conn.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch ( Exception ex)
            {
                conn.Close();
            }
            if(conn.State == ConnectionState.Open)
                conn.Close();
        }

    }
}
